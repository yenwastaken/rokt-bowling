# Introduction to rokt-bowling

This project defines a set of functions needed to implement a scorecard for a ten-pin bowling game. All functions are defined in `src/rokt_bowling/core.clj`

A game of bowling consists of up to 10 frames. Each frame consists of two chances to knock down ten pins. 

If you knock down all ten pins in the first shot, that's known as a "strike". It is typically denoted by an "X". A strike is scored by the total of the next two shots.

If you knock down all ten pins in the second shot, that's known as a "spare". It is typically denoted by a "/".

## Data modelling approach

The game of bowling appears to lend itself well to an event-sourcing approach. Event sourcing is where we record the individual events, and derive the current state (i.e. the "scorecard") by applying some reducing function to the current list of events.

### Shots
The most basic event is the "shot", representing a single roll of the ball. For our purposes, we can model a shot simply as an integer representing the number of pins taken down. This can be a value between 0 and 10. 

A single game of bowling can then be modelled simply as an ordered list of shots. 

(In a more realistic setting, the "shot" model might also include the player who made the shot, and the time at which the shot occurred.)

### Frames
Shots can then be aggregated into frames. With some exceptions, every two shots constitute one frame. There's also a special case with the final tenth frame:
                                                                                                      
1. If the tenth frame is a strike, the player gets to make two additional shots
2. If the tenth frame is a spare, the player gets to make one additional shot  


### Scorecard
A scorecard is the aggregation of all frames and their score for a single given game. A scorecard might look something like the following:

```
Frame:	        1	2	3	4	5	6	7	8	9	10
Result:	        X	7/	7 2	9 /	X	X	X	2 3	6 /	7/3
Frame Score:	20	17	9	20	30	22	15	5	17	13
Running Total:	20	37	46	66	96	118	133	138	155	168
```

Each frame is scored based on the number of pins taken down. There are two special cases:

1. If it's a strike, the frame's score is 10 + the sum of the next two shots
2. If it's a spare, the frame's score is 10 + the sum of the next shot

The fact that strikes and spares are scored by shots, not frames, is the reason why I chose to make the shot the fundamental unit of the scorecard, and not the frame.
