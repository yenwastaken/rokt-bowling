(ns rokt-bowling.core
  "This module defines the public API needed to get me a job at Rokt ^H^H^H^H^H^H^H^H implement a scorecard
  for a ten-pin bowling game.

  This module implements the \"traditional\" scoring method, which means
  1. Each frame's score is the sum of its shots, except for spares and strikes
  2. A strike's score is 10 plus the sum of the next two shots
  3. A spare's score is 10 plus the next shot

  Some terminology:
  - A `shot` represents the number of pins taken down in one given bowl. Should be an integer from 0-10 inclusive.
  - A `frame` represents two chances to knock down ten pins. A complete game of bowling consists of 10 frames.
    For the purposes of the code, a frame consists of
      - The frame number, representing the order of the frame within a game;
      - The shots, which is a list of shots representing the number of pins taken down with the frame.
        There are at most 2 shots, except for the 10th frame which can have up to 3 shots.
      - The score of the frame.
  - An empty frame is a frame with no shots and therefore 0 score
  - A `scorecard` is a list of 10 frames, some or all of which may be empty.
  ")

(defn- calculate-next-frame-score
  "Given the next few shots, this calculates the score for the next frame, taking into
  account the bonus points for spares and strikes"
  [shots]
  (let [[first? second? third? & _] shots
        first (or first? 0)
        second (or second? 0)
        third (or third? 0)]
    (cond
      (= first 10) (+ first second third)
      (= (+ first second) 10) (+ first second third)
      :else (+ first second))))

(defn- to-frames
  "This takes an ordered collection of shots, and recursively builds a vector of frames"
  ([shots] (to-frames shots []))
  ([shots frames]
   (let [[first & rest] shots
         score (calculate-next-frame-score shots)
         frame-number (inc (count frames))]
     (cond
       (nil? first)         frames
       (nil? rest)          (conj frames {:number frame-number :shots [first] :score score})
       (= frame-number 10)  (conj frames {:number frame-number :shots shots :score score})
       (= first 10)         (recur rest (conj frames {:number frame-number :shots [10] :score score}))
       :else (let [[second & remaining] rest]
               (recur remaining (conj frames {:number frame-number :shots [first second] :score score})))))))

(defn- to-shots
  "This takes a scorecard (which is an ordered collection of frames), and returns a vector of shots"
  [scorecard] (apply concat (map :shots scorecard)))

(defn- empty-frame [number] {:number number :shots [] :score 0})
(defn- to-scorecard
  "This takes an ordered collection of frames, and returns a scorecard. A scorecard consists of 10 frames,
  some of them might be empty."
  [frames]
  (let [padding-frame-starting-number (+ 1 (count frames))]
    (->> (iterate inc padding-frame-starting-number)
         (map empty-frame)
         (concat frames)
         (take 10)
         (into []))))

(defn- last-frame? [frame] (= 10 (:number frame)))
(defn- frame-complete?
  "Returns true if there are no more shots left in the frame. Note that special handling is needed for the tenth
  frame"
  [frame]
  (let [shots       (:shots frame)
        shot-counts (count shots)]
    (if (last-frame? frame)
      (cond
        (= 10 (first shots)) (= 3 shot-counts)                ;; Strike
        (= 10 (reduce + (take 2 shots))) (= 3 shot-counts)    ;; Spare
        :else (= 2 shot-counts))
      (cond
        (= 10 (first shots)) true                           ;; Strike
        :else (= 2 shot-counts)))))

(defn- scorecard-complete?
  "Returns true if the scorecard represents a completed game"
  [scorecard]
  (every? frame-complete? scorecard))

(defn- valid-frame?
  "Returns true if the frame represents a valid one.
  Note: This is probably better implemented with clojure.spec, but I am not familiar with that area"
  [frame]
  (let [{number :number
         shots  :shots} frame]
    (and
      (>= number 1)
      (<= number 10)
      (every? #(and (>= %1 0) (<= %1 10)) shots)
      (if (= number 10)
        (cond
          (= 10 (first shots))             (<= (count shots) 3)
          (= 10 (reduce + (take 2 shots))) (<= (count shots) 3)
          :else                            (<= (count shots) 2))
        (and (<= (count shots) 2)
             (<= (reduce + shots) 10))))))

(defn- valid-scorecard?
  "Returns true if the scorecard is a valid one"
  [scorecard]
  (every? valid-frame? scorecard))

;; Public API

(defn empty-score-card
  "(Req. #1) Returns an empty score card"
  []
  (map #(identity {:number %1 :shots [] :score 0}) (range 1 11)))

(defn apply-shots-to-scorecard
  "(Req. #2) Takes a scorecard and the next few shots, and returns a new scorecard with the new shots added.
  Note that this can take an arbitrary number of shots. The shots don't have to be part of the same frame.

  If this result in invalid frames, returns nil"
  [score-card next-shots]
  (as-> score-card v
        (to-shots v)
        (concat v next-shots)
        (to-frames v)
        (to-scorecard v)
        (if (valid-scorecard? v) v nil)))

(defn total-score-from-scorecard
  "(Req. #3) This takes a scorecard, and calculates the total score for the game"
  [scorecard]
  (reduce + (map :score scorecard)))

(defn scorecard-result
  "(Req. #3) This takes a scorecard, and returns the total score, or :incomplete if the game isn't complete"
  [scorecard]
  (if (scorecard-complete? scorecard)
    (total-score-from-scorecard scorecard)
    :incomplete))