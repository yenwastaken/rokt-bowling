(ns rokt-bowling.core-test
  (:require [clojure.test :refer :all]
            [rokt-bowling.core :refer :all]))

(defn- shots->scorecard [shots] (apply-shots-to-scorecard (empty-score-card) shots))
(deftest test-empty-score-card
  (testing "The return type of the empty score card should be a list of 10 empty frames"
    (is (= (empty-score-card) '({:number 1, :shots [], :score 0}
                                 {:number 2, :shots [], :score 0}
                                 {:number 3, :shots [], :score 0}
                                 {:number 4, :shots [], :score 0}
                                 {:number 5, :shots [], :score 0}
                                 {:number 6, :shots [], :score 0}
                                 {:number 7, :shots [], :score 0}
                                 {:number 8, :shots [], :score 0}
                                 {:number 9, :shots [], :score 0}
                                 {:number 10, :shots [], :score 0})))))

(deftest test-perfect-game
  (testing "The perfect game consists of 12 consecutive strikes (10 frames, with the last frame
  having 3 shots"
    (let [scorecard (shots->scorecard [10 10 10 10 10 10 10 10 10 10 10 10])]
      (is (= scorecard [{:number 1 :score 30 :shots [10]}
                        {:number 2 :score 30 :shots [10]}
                        {:number 3 :score 30 :shots [10]}
                        {:number 4 :score 30 :shots [10]}
                        {:number 5 :score 30 :shots [10]}
                        {:number 6 :score 30 :shots [10]}
                        {:number 7 :score 30 :shots [10]}
                        {:number 8 :score 30 :shots [10]}
                        {:number 9 :score 30 :shots [10]}
                        {:number 10 :score 30 :shots [10 10 10]}]))
      (is (= (total-score-from-scorecard scorecard) 300)))))

(deftest test-bad-game
  (testing "The worst game consists of a streak of 20 consecutive gutter balls (0s)")
  (let [scorecard (shots->scorecard (repeat 20 0))]
    (is (= scorecard [{:number 1 :score 0 :shots [0 0]}
                      {:number 2 :score 0 :shots [0 0]}
                      {:number 3 :score 0 :shots [0 0]}
                      {:number 4 :score 0 :shots [0 0]}
                      {:number 5 :score 0 :shots [0 0]}
                      {:number 6 :score 0 :shots [0 0]}
                      {:number 7 :score 0 :shots [0 0]}
                      {:number 8 :score 0 :shots [0 0]}
                      {:number 9 :score 0 :shots [0 0]}
                      {:number 10 :score 0 :shots [0 0]}]))
    (is (= (total-score-from-scorecard scorecard) 0))))

(deftest test-apply-shots-to-scorecard
  (is (= (apply-shots-to-scorecard [{:number 1, :shots [3 4], :score 7}
                                    {:number 2, :shots [], :score 0}
                                    {:number 3, :shots [], :score 0}
                                    {:number 4, :shots [], :score 0}
                                    {:number 5, :shots [], :score 0}
                                    {:number 6, :shots [], :score 0}
                                    {:number 7, :shots [], :score 0}
                                    {:number 8, :shots [], :score 0}
                                    {:number 9, :shots [], :score 0}
                                    {:number 10, :shots [], :score 0}] [5 5])
         [{:number 1, :shots [3 4], :score 7}
          {:number 2, :shots [5 5], :score 10}
          {:number 3, :shots [], :score 0}
          {:number 4, :shots [], :score 0}
          {:number 5, :shots [], :score 0}
          {:number 6, :shots [], :score 0}
          {:number 7, :shots [], :score 0}
          {:number 8, :shots [], :score 0}
          {:number 9, :shots [], :score 0}
          {:number 10, :shots [], :score 0}])
      "New shots should be added to next empty frame")
  (is (= (apply-shots-to-scorecard [{:number 1, :shots [3 4], :score 7}
                                    {:number 2, :shots [], :score 0}
                                    {:number 3, :shots [], :score 0}
                                    {:number 4, :shots [], :score 0}
                                    {:number 5, :shots [], :score 0}
                                    {:number 6, :shots [], :score 0}
                                    {:number 7, :shots [], :score 0}
                                    {:number 8, :shots [], :score 0}
                                    {:number 9, :shots [], :score 0}
                                    {:number 10, :shots [], :score 0}] [5 1 7 0])
         [{:number 1, :shots [3 4], :score 7}
          {:number 2, :shots [5 1], :score 6}
          {:number 3, :shots [7 0], :score 7}
          {:number 4, :shots [], :score 0}
          {:number 5, :shots [], :score 0}
          {:number 6, :shots [], :score 0}
          {:number 7, :shots [], :score 0}
          {:number 8, :shots [], :score 0}
          {:number 9, :shots [], :score 0}
          {:number 10, :shots [], :score 0}])
      "New shots can overflow to more than one frame")
  (is (= (apply-shots-to-scorecard [{:number 1, :shots [10], :score 10}
                                    {:number 2, :shots [], :score 0}
                                    {:number 3, :shots [], :score 0}
                                    {:number 4, :shots [], :score 0}
                                    {:number 5, :shots [], :score 0}
                                    {:number 6, :shots [], :score 0}
                                    {:number 7, :shots [], :score 0}
                                    {:number 8, :shots [], :score 0}
                                    {:number 9, :shots [], :score 0}
                                    {:number 10, :shots [], :score 0}] [4 5 6])
         [{:number 1, :shots [10], :score 19}
          {:number 2, :shots [4 5], :score 9}
          {:number 3, :shots [6], :score 6}
          {:number 4, :shots [], :score 0}
          {:number 5, :shots [], :score 0}
          {:number 6, :shots [], :score 0}
          {:number 7, :shots [], :score 0}
          {:number 8, :shots [], :score 0}
          {:number 9, :shots [], :score 0}
          {:number 10, :shots [], :score 0}])
      "Strike scores get updated with the next 2 shots")
  (is (= (apply-shots-to-scorecard [{:number 1, :shots [7 3], :score 10}
                                    {:number 2, :shots [], :score 0}
                                    {:number 3, :shots [], :score 0}
                                    {:number 4, :shots [], :score 0}
                                    {:number 5, :shots [], :score 0}
                                    {:number 6, :shots [], :score 0}
                                    {:number 7, :shots [], :score 0}
                                    {:number 8, :shots [], :score 0}
                                    {:number 9, :shots [], :score 0}
                                    {:number 10, :shots [], :score 0}] [4 5 6])
         [{:number 1, :shots [7 3], :score 14}
          {:number 2, :shots [4 5], :score 9}
          {:number 3, :shots [6], :score 6}
          {:number 4, :shots [], :score 0}
          {:number 5, :shots [], :score 0}
          {:number 6, :shots [], :score 0}
          {:number 7, :shots [], :score 0}
          {:number 8, :shots [], :score 0}
          {:number 9, :shots [], :score 0}
          {:number 10, :shots [], :score 0}])
      "Spare scores get updated with the next shot")
  (is (= (apply-shots-to-scorecard [{:number 1, :shots [7 3], :score 10}
                                    {:number 2, :shots [], :score 0}
                                    {:number 3, :shots [], :score 0}
                                    {:number 4, :shots [], :score 0}
                                    {:number 5, :shots [], :score 0}
                                    {:number 6, :shots [], :score 0}
                                    {:number 7, :shots [], :score 0}
                                    {:number 8, :shots [], :score 0}
                                    {:number 9, :shots [], :score 0}
                                    {:number 10, :shots [], :score 0}] [5 6])
         nil)
      "Frames whose shots sum to more than 10 are invalid")
  (is (= (apply-shots-to-scorecard [{:number 1, :shots [3 4], :score 7}
                                    {:number 2, :shots [8 1], :score 9}
                                    {:number 3, :shots [0 0], :score 0}
                                    {:number 4, :shots [9 1], :score 20}
                                    {:number 5, :shots [10], :score 16}
                                    {:number 6, :shots [6 0], :score 6}
                                    {:number 7, :shots [4 4], :score 8}
                                    {:number 8, :shots [3 6], :score 9}
                                    {:number 9, :shots [2 8], :score 18}
                                    {:number 10, :shots [10 10 10], :score 30}] [5])
         nil)
      "Adding more shots to a completed game is invalid"))


(deftest test-final-result
  (is (= (scorecard-result [{:number 1, :shots [7 3], :score 14}
                            {:number 2, :shots [4 5], :score 9}
                            {:number 3, :shots [6], :score 6}
                            {:number 4, :shots [], :score 0}
                            {:number 5, :shots [], :score 0}
                            {:number 6, :shots [], :score 0}
                            {:number 7, :shots [], :score 0}
                            {:number 8, :shots [], :score 0}
                            {:number 9, :shots [], :score 0}
                            {:number 10, :shots [], :score 0}])
         :incomplete)
      "Incomplete games get :incomplete")
  (is (= (scorecard-result [{:number 1, :shots [3 4], :score 7}
                            {:number 2, :shots [8 1], :score 9}
                            {:number 3, :shots [0 0], :score 0}
                            {:number 4, :shots [9 1], :score 20}
                            {:number 5, :shots [10], :score 16}
                            {:number 6, :shots [6 0], :score 6}
                            {:number 7, :shots [4 4], :score 8}
                            {:number 8, :shots [3 6], :score 9}
                            {:number 9, :shots [2 8], :score 18}
                            {:number 10, :shots [8 0], :score 8}])
         101)
      "A simple completed game gets a score")
  (is (= (scorecard-result [{:number 1, :shots [3 4], :score 7}
                            {:number 2, :shots [8 1], :score 9}
                            {:number 3, :shots [0 0], :score 0}
                            {:number 4, :shots [9 1], :score 20}
                            {:number 5, :shots [10], :score 16}
                            {:number 6, :shots [6 0], :score 6}
                            {:number 7, :shots [4 4], :score 8}
                            {:number 8, :shots [3 6], :score 9}
                            {:number 9, :shots [2 8], :score 18}
                            {:number 10, :shots [8 2], :score 10}])
         :incomplete)
      "If the last frame is a spare, player gets one more shot")
  (is (= (scorecard-result [{:number 1, :shots [3 4], :score 7}
                            {:number 2, :shots [8 1], :score 9}
                            {:number 3, :shots [0 0], :score 0}
                            {:number 4, :shots [9 1], :score 20}
                            {:number 5, :shots [10], :score 16}
                            {:number 6, :shots [6 0], :score 6}
                            {:number 7, :shots [4 4], :score 8}
                            {:number 8, :shots [3 6], :score 9}
                            {:number 9, :shots [2 8], :score 18}
                            {:number 10, :shots [10], :score 10}])
         :incomplete)
      "If the last frame is a strike, player gets two more shots"))